package com.nemanja.starbux.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Nemanja.Tomanovic created on 05-Mar-20
 **/
@Entity
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Customers customer;
    private BigDecimal totalAmount;
    private BigDecimal discountAmount;
    private BigDecimal amountAfterDiscount;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "order")
    private List<OrdersItem> ordersItem = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customers getCustomer() {
        return customer;
    }

    public void setCustomer(Customers customer) {
        this.customer = customer;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getAmountAfterDiscount() {
        return amountAfterDiscount;
    }

    public void setAmountAfterDiscount(BigDecimal amountAfterDiscount) {
        this.amountAfterDiscount = amountAfterDiscount;
    }

    public List<OrdersItem> getOrdersItem() {
        return ordersItem;
    }

    public void setOrdersItem(List<OrdersItem> ordersItem) {
        this.ordersItem = ordersItem;
    }
}
