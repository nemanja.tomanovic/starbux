package com.nemanja.starbux.service.implementation;

import com.nemanja.starbux.model.Drinks;
import com.nemanja.starbux.repositories.DrinksRepository;
import com.nemanja.starbux.service.DrinksService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Nemanja.Tomanovic created on 05-Mar-20
 **/
@Service
public class DrinkServiceImpl implements DrinksService {


    private DrinksRepository drinksRepository;
    private Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

    public DrinkServiceImpl(DrinksRepository drinksRepository) {
        this.drinksRepository = drinksRepository;
    }

    @Override
    public Optional<Drinks> findDrink(Long id) {
        logger.info("Fetching drink with id {}", id);
        return drinksRepository.findById(id);
    }

    @Override
    public Optional<Drinks> saveDrink(Drinks drinks) {
        drinksRepository.save(drinks);
        logger.info("Saving drink with id {}", drinks.getId());
        return findDrink(drinks.getId());
    }

    @Override
    public boolean updateDrink(Drinks drink) {
        if (checkUpdate(drink)) {
            try {
                logger.debug("Update drink with id {}", drink.getId());
                drinksRepository.save(drink);
                return true;
            } catch (Exception ex) {
                logger.error("Exception during update of a drink");
                return false;
            }
        } else {
            logger.info("You need to pass existing drink for update");
            return false;
        }
    }

    @Override
    public boolean deleteDrink(Long id) {
        try {
            drinksRepository.deleteById(id);
            logger.info("Removing drink with id {}",id);
            return true;
        } catch (Exception ex) {
            logger.error("Exception during deleting a drink");
            return false;
        }
    }

    @Override
    public boolean checkUpdate(Drinks drinks) {
        return findDrink(drinks.getId()).isPresent();
    }
}
