package com.nemanja.starbux.controller;


import com.nemanja.starbux.dto.MostUsedToppingDto;
import com.nemanja.starbux.dto.OrdersPerCustomerDto;
import com.nemanja.starbux.service.OrdersItemService;
import com.nemanja.starbux.service.OrdersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Class that represent controller used for generating reports for admins o application
 * Comes with two methods that are used for generating reports about orders per customer
 * and most used toppings
 *
 * @author Nemanja.Tomanovic
 */
@RestController
public class ReportController {

    private OrdersService ordersService;
    private OrdersItemService ordersItemService;
    private Logger logger = LoggerFactory.getLogger(ReportController.class);

    public ReportController(OrdersService ordersService, OrdersItemService ordersItemService) {
        this.ordersService = ordersService;
        this.ordersItemService = ordersItemService;
    }

    /**
     * Method used to generate orders per customer report based on id provided
     * @param id id of customer which number of orders is requested
     * @return information about customers orders including number of orders and total amount of them
     */
    @GetMapping("get/ordersPerCustomer/{id}")
    public OrdersPerCustomerDto getTotalOrders(@PathVariable("id")Long id){
        logger.debug("Generating Orders per customer report for customer with id {}", id);
        return ordersService.findOrdersByCustomer(id);
    }

    /**
     * Method used to generate most used toppings report
     *
     * @return list of objects with most used toppings information. List is returned because there can be several toppings that are most used
     */
    @GetMapping("mostUsedToppings")
    public List<MostUsedToppingDto> getTopping(){
        logger.debug("Generating Most used toppings reports");
        return ordersItemService.getTopping();
    }
}
