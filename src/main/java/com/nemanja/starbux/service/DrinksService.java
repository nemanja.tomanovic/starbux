package com.nemanja.starbux.service;

import com.nemanja.starbux.model.Drinks;

import java.util.Optional;


public interface DrinksService {
    Optional<Drinks> findDrink(Long id);
    Optional<Drinks> saveDrink(Drinks drinks);
    boolean updateDrink(Drinks drink);
    boolean deleteDrink(Long id);
    boolean checkUpdate(Drinks drinks);
}
