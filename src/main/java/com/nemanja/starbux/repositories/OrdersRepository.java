package com.nemanja.starbux.repositories;

import com.nemanja.starbux.model.Orders;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface OrdersRepository extends CrudRepository<Orders, Long> {

    List<Orders> findAllByCustomerId(Long id);
}
