package com.nemanja.starbux.dto;

/**
 * DTO used when generating report for most used drinks.
 * Used closed interface based projection because we need only part of original entity
 *
 * @author Nemanja.Tomanovic
 */
public interface MostUsedToppingDto {
     String getItem();
     int getOccurrence();
}
