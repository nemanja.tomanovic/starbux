package com.nemanja.starbux.repositories;

import com.nemanja.starbux.model.Drinks;
import org.springframework.data.repository.CrudRepository;

public interface DrinksRepository extends CrudRepository<Drinks, Long> {
}
