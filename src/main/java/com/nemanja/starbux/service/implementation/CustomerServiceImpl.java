package com.nemanja.starbux.service.implementation;

import com.nemanja.starbux.model.Customers;
import com.nemanja.starbux.repositories.CustomersRepository;
import com.nemanja.starbux.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    private Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

    private CustomersRepository customersRepository;

    public CustomerServiceImpl(CustomersRepository customersRepository) {
        this.customersRepository = customersRepository;
    }

    @Override
    public Optional<Customers> findCustomer(Long id) {
        logger.debug("Trying to find customer with id {}", id);
        return customersRepository.findById(id);
    }

    @Override
    public Optional<Customers> saveCustomer(Customers customer) {
        logger.debug("Saving customer with id {}", customer.getId());
        customersRepository.save(customer);
        return findCustomer(customer.getId());
    }

    @Override
    public boolean updateCustomer(Customers customers) {
        if (checkUpdate(customers)){
            try {
                logger.debug("Update customer with id {}", customers.getId());
                customersRepository.save(customers);
                return true;
            }catch (Exception ex){
                logger.error("Error while updating customer");
                return false;
            }
        }else {
            logger.info("You need to pass existing customer for update");
            return false;
        }
    }

    @Override
    public boolean checkUpdate(Customers customers) {
        return findCustomer(customers.getId()).isPresent();
    }
}
