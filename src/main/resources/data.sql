INSERT INTO drinks (drink_name, price, description) VALUES
('Black Coffee', 4.00, 'Strong black coffee with rich aroma'),
('Latte', 5.00, 'Perfect combination of quality espresso and steamed milk'),
('Mocha', 6.00, 'Chocolate-flavored relative of Latte coffee'),
('Tea', 3.00, 'Various flavours of quality tea blends');

INSERT INTO toppings(topping_name, price, description) VALUES
('Milk', 2.00, 'Steamed or cold milk as classic addition to any coffee'),
('Hazelnut syrup', 3.00, 'Taste of autumn in a cup of coffee'),
('Chocolate sauce', 5.00, 'Adds richness to the taste of coffee'),
('Lemon', 2.00, 'Fresh slice of lemon for your cup of tea');

INSERT INTO customers(first_name, last_name, age, email) VALUES
('Lily', 'Aldrin', 32, 'lily@himym.com'),
('Tony', 'Stark', 43, 'stark@avenger.com'),
('Eddie', 'Brock', 38, 'eddie@venom.com'),
('Natasha', 'Romanova', 35, 'nat@widow.com');
