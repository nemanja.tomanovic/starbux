package com.nemanja.starbux.service.implementation;

import com.nemanja.starbux.dto.CartDto;
import com.nemanja.starbux.dto.MostUsedToppingDto;
import com.nemanja.starbux.model.OrdersItem;
import com.nemanja.starbux.repositories.OrdersItemRepository;
import com.nemanja.starbux.service.OrdersItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * Nemanja.Tomanovic created on 06-Mar-20
 **/
@Service
public class OrdersItemServiceImpl implements OrdersItemService {

    private List<OrdersItem> listOfItems = new ArrayList<>();
    private OrdersItemRepository ordersItemRepository;
    private Logger logger = LoggerFactory.getLogger(OrdersItemServiceImpl.class);

    public OrdersItemServiceImpl(OrdersItemRepository ordersItemRepository) {
        this.ordersItemRepository = ordersItemRepository;
    }


    @Override
    public CartDto addSingleItem(OrdersItem item) {
        logger.info("Adding new item: {}", item.getItemName());
        CartDto cart = new CartDto();
        listOfItems.add(item);
        if (listOfItems.size() > 0){
            listOfItems.forEach(k -> cart.getProducts().add(k.getItemName()));
            cart.setCurrentAmount(listOfItems.stream().map(OrdersItem::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add));
            return cart;
        }else {
            return new CartDto();
        }
    }

    @Override
    public List<OrdersItem> getOrdersForFinalizing() {
        logger.info("Finalizing the order !");
        List<OrdersItem> finalList = new ArrayList<>();
        finalList.addAll(listOfItems);
        listOfItems.clear();
        return finalList;
    }

    public List<MostUsedToppingDto> getTopping() {

        logger.debug("Fetching most used topping");
        List<MostUsedToppingDto> listOfTopping = ordersItemRepository.getToppings();

        Optional<MostUsedToppingDto> occur = listOfTopping.stream().max(Comparator.comparing(MostUsedToppingDto::getOccurrence));
        MostUsedToppingDto max = null ;
        if (occur.isPresent()){
            max = occur.get();
        }
       if (max != null){

           List<MostUsedToppingDto> mostUsedToppings = new ArrayList<>();

           for (MostUsedToppingDto mutd : listOfTopping){
               if (mutd.getOccurrence() == max.getOccurrence()){
                   mostUsedToppings.add(mutd);
               }
           }
           return mostUsedToppings;
       }else {
           logger.info("Couldn't find any toppings");
           return new ArrayList<>();
       }

    }
}
