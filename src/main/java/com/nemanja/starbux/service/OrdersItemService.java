package com.nemanja.starbux.service;

import com.nemanja.starbux.dto.CartDto;
import com.nemanja.starbux.dto.MostUsedToppingDto;
import com.nemanja.starbux.model.OrdersItem;
import java.util.List;

public interface OrdersItemService {

    CartDto addSingleItem(OrdersItem item);
    List<OrdersItem> getOrdersForFinalizing();
    List<MostUsedToppingDto> getTopping();
}
