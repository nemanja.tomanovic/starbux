#1. Build : docker build -t nemanjat 
#2. Run: docker run -d -p 8088:8088 -i -t nemanjat 
FROM openjdk:8
COPY ./target/starbux-0.0.1-SNAPSHOT.jar myapp/starbux-0.0.1-SNAPSHOT.jar
WORKDIR /myapp
EXPOSE 8088
CMD java -jar starbux-0.0.1-SNAPSHOT.jar