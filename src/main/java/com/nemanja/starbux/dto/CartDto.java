package com.nemanja.starbux.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO used for representing current state of cart
 * during process of adding items to order
 *
 * @author Nemanja.Tomanovic
 */
public class CartDto {

    private List<String> products = new ArrayList<>();
    private BigDecimal currentAmount;

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }

    public BigDecimal getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(BigDecimal currentAmount) {
        this.currentAmount = currentAmount;
    }
}
