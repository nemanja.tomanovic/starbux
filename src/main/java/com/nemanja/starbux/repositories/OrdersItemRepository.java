package com.nemanja.starbux.repositories;

import com.nemanja.starbux.dto.MostUsedToppingDto;
import com.nemanja.starbux.model.OrdersItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface OrdersItemRepository extends CrudRepository<OrdersItem, Long> {

    @Query(value = "select item_name as item, count(item_name) as occurrence\n" +
            "from orders_item \n" +
            "where is_topping = true\n" +
            "group by item\n" +
            "order by occurrence DESC", nativeQuery = true)
    List<MostUsedToppingDto> getToppings();
}
