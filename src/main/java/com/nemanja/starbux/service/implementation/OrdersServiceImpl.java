package com.nemanja.starbux.service.implementation;

import com.nemanja.starbux.dto.FinalOrderDto;
import com.nemanja.starbux.dto.OrdersPerCustomerDto;
import com.nemanja.starbux.model.Orders;
import com.nemanja.starbux.model.OrdersItem;
import com.nemanja.starbux.repositories.CustomersRepository;
import com.nemanja.starbux.repositories.OrdersRepository;
import com.nemanja.starbux.service.OrdersItemService;
import com.nemanja.starbux.service.OrdersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

/**
 * Nemanja.Tomanovic created on 06-Mar-20
 **/
@Service
public class OrdersServiceImpl implements OrdersService {

    private OrdersRepository ordersRepository;
    private OrdersItemService ordersItemService;
    private CustomersRepository customersRepository;
    private Logger logger = LoggerFactory.getLogger(OrdersServiceImpl.class);

    public OrdersServiceImpl(OrdersRepository ordersRepository, OrdersItemService ordersItemService, CustomersRepository customersRepository) {
        this.ordersRepository = ordersRepository;
        this.ordersItemService = ordersItemService;
        this.customersRepository = customersRepository;
    }

    @Override
    public FinalOrderDto createOrder(Long customerId) {
        Orders order = new Orders();
        FinalOrderDto finalOrderDto = new FinalOrderDto();
        logger.debug("Creating order for customer with id {}", customerId);
        List<OrdersItem> ordersItems = ordersItemService.getOrdersForFinalizing();
        order.setCustomer(customersRepository.findById(customerId).get());
        order.setOrdersItem(ordersItems);
        order.setTotalAmount(ordersItems.stream().map(OrdersItem::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add));
        order.setAmountAfterDiscount(amountDiscount(ordersItems).compareTo(itemDiscount(ordersItems)) > 0 ? itemDiscount(ordersItems) : amountDiscount(ordersItems));
        order.setDiscountAmount(ordersItems.stream().map(OrdersItem::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add).subtract(order.getAmountAfterDiscount()));
        ordersItems.forEach(item -> item.setOrder(order));

        ordersRepository.save(order);

        finalOrderDto.setOriginalAmount(order.getTotalAmount());
        finalOrderDto.setDiscountedAmount(order.getAmountAfterDiscount());
        return finalOrderDto;
    }

    @Override
    public OrdersPerCustomerDto findOrdersByCustomer(Long id) {
        logger.debug("Finding all orders for customer with id {}", id);
        OrdersPerCustomerDto ordersPerCustomerDto = new OrdersPerCustomerDto();

        List<Orders> listOfOrders = ordersRepository.findAllByCustomerId(id);
        if (listOfOrders.size() != 0){
            ordersPerCustomerDto.setFirstName(listOfOrders.get(0).getCustomer().getFirstName());
            ordersPerCustomerDto.setLastName(listOfOrders.get(0).getCustomer().getLastName());
            ordersPerCustomerDto.setId(listOfOrders.get(0).getCustomer().getId());
            ordersPerCustomerDto.setOrdersAmount(listOfOrders.size());
            ordersPerCustomerDto.setTotalAmount(listOfOrders.stream().map(Orders::getAmountAfterDiscount).reduce(BigDecimal.ZERO, BigDecimal::add));
            return ordersPerCustomerDto;
        }else {
            logger.info("Customer with id {} has no orders", id);
            return new OrdersPerCustomerDto();
        }
    }

    /**
     * Method used to calculate amount based discount used in finalizing orders
     *
     * @param items list of items in order
     * @return discounted value of items that is calculated if sum of all items values is higher then 12,
     * or original amount if total sum of items value is less then 12
     */
    private BigDecimal amountDiscount(List<OrdersItem> items) {

        BigDecimal amount = items.stream().map(OrdersItem::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);

        if (amount.compareTo(BigDecimal.valueOf(12)) > 0) {
            amount = amount.multiply(BigDecimal.valueOf(0.75));
        }

        return amount;
    }


    /**
     * Method used to calculate item based discount used in finalizing orders
     *
     * @param items list of items in order
     * @return discounted value of items that is calculated if number of items in order is equals or higher then 3,
     * or original amount if number of items in order is less then 3
     */
    private BigDecimal itemDiscount(List<OrdersItem> items) {
        BigDecimal amount = items.stream().map(OrdersItem::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        if (items.size() >= 3) {
            amount = amount.subtract(items.stream().map(OrdersItem::getPrice).min(Comparator.naturalOrder()).get());
        }

        return amount;
    }
}
