package com.nemanja.starbux.controller;

import com.nemanja.starbux.dto.CartDto;
import com.nemanja.starbux.model.OrdersItem;
import com.nemanja.starbux.service.OrdersItemService;
import org.springframework.web.bind.annotation.*;

/**
 * Class that represent controller used for manipulating orders item data.
 * Comes with one method that is used for adding new item to order
 *
 * @author Nemanja.Tomanovic
 */
@RestController
public class OrdersItemController {

    private OrdersItemService ordersItemService;

    public OrdersItemController(OrdersItemService ordersItemService) {
        this.ordersItemService = ordersItemService;
    }

    /**
     * Method used when customer adds new drink or topping to order
     * @param item information about new item that is added, should come as body of request with information detailed here {@link com.nemanja.starbux.model.OrdersItem}
     * @return list of all items in the cart at the moment of acquisition with amount of cart or empty object if there is nothing in the cart
     */
    @PostMapping("add/orders/item")
    public CartDto addSingleItem(@RequestBody OrdersItem item){
        return ordersItemService.addSingleItem(item);
    }


}
