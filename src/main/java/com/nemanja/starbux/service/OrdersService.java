package com.nemanja.starbux.service;

import com.nemanja.starbux.dto.FinalOrderDto;
import com.nemanja.starbux.dto.OrdersPerCustomerDto;

/**
 * Nemanja.Tomanovic created on 06-Mar-20
 **/
public interface OrdersService {
    OrdersPerCustomerDto findOrdersByCustomer(Long id);
    FinalOrderDto createOrder(Long customerId);
}
