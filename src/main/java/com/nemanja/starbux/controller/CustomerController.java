package com.nemanja.starbux.controller;

import com.nemanja.starbux.model.Customers;
import com.nemanja.starbux.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Class that represent controller used for manipulating customer data.
 * Used for finding, adding and updating customer.
 *
 * @author Nemanja.Tomanovic
 */
@RestController
public class CustomerController {

    private CustomerService customerService;

    private Logger logger = LoggerFactory.getLogger(CustomerController.class);

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    /**
     * Method used for adding new customer.
     * Four original customers are added at initialization of application but using this method we can add more.
     * @param newCustomer information about new customer, should come as body of request with information detailed here {@link com.nemanja.starbux.model.Customers}
     * @return customer that is created or empty object if there was an error during process of saving new customer
     */
    @PostMapping("add/customers")
    public Customers addCustomer(@RequestBody Customers newCustomer) {
        Optional<Customers> customer = customerService.saveCustomer(newCustomer);
        if (customer.isPresent()) {
            return customer.get();
        } else {
            logger.error("Exception while adding new customer");
            return new Customers();
        }
    }

    /**
     * Method used for finding information about specific customer using id.
     *
     * @param id Id of customer that we want information about
     * @return Customer object that was found by id provided or empty object if nothing was found
     */
    @GetMapping("customers/{id}")
    public Customers findCustomer(@PathVariable("id") Long id) {
        Optional<Customers> customer = customerService.findCustomer(id);

        if (customer.isPresent()) {
            return customer.get();
        } else {
            logger.error("Haven't found requested customer with id {}", id);
            return new Customers();
        }
    }

    /**
     * Method used when we want to update existing customer. Method checks if record exists in database by id and will not perform
     * update if there is  nothing to be updated
     *
     * @param customer Customer object that will be used for updating. Should come as body of request with information detailed here {@link com.nemanja.starbux.model.Customers}
     * @return True if update was successful or false if customer was not found in database or there was an error updating customer
     */
    @PostMapping("update/customers")
    public boolean updateCustomer(@RequestBody Customers customer) {

        return customerService.updateCustomer(customer);
    }
}
