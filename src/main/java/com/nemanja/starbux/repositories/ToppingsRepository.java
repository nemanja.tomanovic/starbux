package com.nemanja.starbux.repositories;

import com.nemanja.starbux.model.Toppings;
import org.springframework.data.repository.CrudRepository;

public interface ToppingsRepository extends CrudRepository<Toppings, Long> {
}
