package com.nemanja.starbux.controller;


import com.nemanja.starbux.model.Drinks;
import com.nemanja.starbux.service.DrinksService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

/**
 * Class that represent controller used for manipulating drinks data.
 * Supports adding, finding, deleting and updating drinks information
 *
 * @author Nemanja.Tomanovic
 */
@RestController
public class DrinkController {

    private DrinksService drinksService;

    private Logger logger = LoggerFactory.getLogger(DrinkController.class);
    public DrinkController(DrinksService drinksService) {
        this.drinksService = drinksService;
    }

    /**
     * Method used for adding new drink into database
     * Four original drinks are added at initialization of application but using this method we can add more
     * @param newDrink information about new drink that is added, should come as body of request with information detailed here {@link com.nemanja.starbux.model.Drinks}
     * @return new drink that we created or empty object if there was an error during the process of saving
     */
    @PostMapping("/add/drinks")
    public Drinks addDrink(@RequestBody Drinks newDrink){
        Optional<Drinks> drink = drinksService.saveDrink(newDrink);
        if (drink.isPresent()){
            return drink.get();
        }else{
            logger.error("Exception while adding new drink");
            return new Drinks();
        }
    }

    /**
     * Method used for finding drink information based on passed id
     * @param id id of drink that we want information about
     * @return Drinks object that was found by id provided or empty object if nothing was found
     */
    @GetMapping("drinks/{id}")
    public Drinks findDrink(@PathVariable("id") Long id) {
        Optional<Drinks> drink = drinksService.findDrink(id);
        if (drink.isPresent()) {
            return drink.get();
        } else {
            logger.error("Exception while fetching drink with id {}", id);
            return new Drinks();
        }
    }

    /**
     * Method used for removing drink from a database using id provided
     * @param id id of a drink that we want to be removed from database
     * @return true removing was successful or false if there was an error during deleting
     */
    @GetMapping("/delete/drinks/{id}")
    public boolean deleteDrink(@PathVariable("id") Long id){
        return drinksService.deleteDrink(id);
    }

    /**
     * Method used when we want to update existing drink. Method checks if record exists in database by id and will not perform
     * update if there is  nothing to be updated
     *
     * @param drinks Drinks object that will be used for updating. Should come as body of request with information detailed here {@link com.nemanja.starbux.model.Drinks}
     * @return True if update was successful or false if drink was not found in database or there was an error updating drink
     */
    @PostMapping("/update/drinks")
    public boolean updateDrink(@RequestBody Drinks drinks){
        return drinksService.updateDrink(drinks);
    }
}
