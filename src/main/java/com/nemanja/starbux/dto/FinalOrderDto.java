package com.nemanja.starbux.dto;

import java.math.BigDecimal;

/**
 * DTO used for representing order when we finish the
 * process of ordering
 *
 * @author Nemanja.Tomanovic
 */
public class FinalOrderDto {

    private BigDecimal originalAmount;
    private BigDecimal discountedAmount;


    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
    }

    public BigDecimal getDiscountedAmount() {
        return discountedAmount;
    }

    public void setDiscountedAmount(BigDecimal discountedAmount) {
        this.discountedAmount = discountedAmount;
    }
}
