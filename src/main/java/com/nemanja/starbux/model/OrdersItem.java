package com.nemanja.starbux.model;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Nemanja.Tomanovic created on 05-Mar-20
 **/
@Entity
public class OrdersItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Orders order;
    private String itemName;
    private BigDecimal price;
    private boolean isTopping;


    public OrdersItem() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Orders getOrder() {
        return order;
    }

    public void setOrder(Orders order) {
        this.order = order;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isTopping() {
        return isTopping;
    }

    public void setTopping(boolean topping) {
        isTopping = topping;
    }
}
