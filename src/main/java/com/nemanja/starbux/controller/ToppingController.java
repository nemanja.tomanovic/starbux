package com.nemanja.starbux.controller;

import com.nemanja.starbux.model.Toppings;
import com.nemanja.starbux.service.ToppingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Class that represent controller used for manipulating toppings data.
 * Supports adding, finding, deleting and updating toppings information
 *
 * @author Nemanja.Tomanovic
 */
@RestController()
public class ToppingController {

    private ToppingsService toppingsService;
    private Logger logger = LoggerFactory.getLogger(ToppingController.class);

    public ToppingController(ToppingsService toppingsService) {
        this.toppingsService = toppingsService;
    }

    /**
     * Method used for adding new topping into database
     * Four original toppings are added at initialization of application but using this method we can add more
     * @param newTopping information about new topping that is added, should come as body of request with information detailed here {@link com.nemanja.starbux.model.Toppings}
     * @return new topping that we created or empty object if there was an error during the process of saving
     */
    @PostMapping("/add/toppings")
    public Toppings addTopping(@RequestBody Toppings newTopping){
        Optional<Toppings> topping = toppingsService.saveTopping(newTopping);
        if (topping.isPresent()){
            return topping.get();
        }else{
            logger.error("Exception while adding new topping");
            return new Toppings();
        }
    }

    /**
     * Method used for finding topping information based on passed id
     * @param id id of topping that we want information about
     * @return Toppings object that was found by id provided or empty object if nothing was found
     */
    @GetMapping("toppings/{id}")
    public Toppings findTopping(@PathVariable("id") Long id){
        Optional<Toppings> topping = toppingsService.findTopping(id);
        if(topping.isPresent()){
            return topping.get();
        }else{
            logger.error("Haven't found requested topping with id {}", id);
            throw new RuntimeException("Haven't found requested topping with id " +id);
        }
    }

    /**
     * Method used for removing topping from a database using id provided
     * @param id id of a topping that we want to be removed from database
     * @return true removing was successful or false if there was an error during deleting
     */
    @GetMapping("delete/toppings/{id}")
    public Boolean deleteById(@PathVariable("id")Long id){
        return toppingsService.deleteToppingById(id);
    }

    /**
     * Method used when we want to update existing topping. Method checks if record exists in database by id and will not perform
     * update if there is  nothing to be updated
     *
     * @param toppings Toppings object that will be used for updating. Should come as body of request with information detailed here {@link com.nemanja.starbux.model.Toppings}
     * @return True if update was successful or false if topping was not found in database or there was an error updating topping
     */
    @PostMapping("update/toppings")
    public Boolean updateTopping(@RequestBody Toppings toppings){
        return toppingsService.updateTopping(toppings);
    }
}
