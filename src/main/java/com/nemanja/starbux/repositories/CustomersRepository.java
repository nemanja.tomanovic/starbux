package com.nemanja.starbux.repositories;

import com.nemanja.starbux.model.Customers;
import org.springframework.data.repository.CrudRepository;

public interface CustomersRepository extends CrudRepository<Customers, Long> {
}
