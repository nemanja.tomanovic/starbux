package com.nemanja.starbux.service;

import com.nemanja.starbux.model.Customers;

import java.util.Optional;

public interface CustomerService {
    Optional<Customers> findCustomer(Long id);
    Optional<Customers> saveCustomer(Customers customer);
    boolean updateCustomer(Customers customers);
    boolean checkUpdate (Customers customers);
}
