package com.nemanja.starbux.service;

import com.nemanja.starbux.model.Toppings;

import java.util.Optional;

public interface ToppingsService {
    Optional<Toppings> findTopping(Long id);
    Optional<Toppings> saveTopping(Toppings toppings);
    boolean deleteToppingById(Long id);
    boolean updateTopping(Toppings toppings);
    boolean checkUpdate(Toppings toppings);
}
