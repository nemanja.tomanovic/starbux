package com.nemanja.starbux.controller;

import com.nemanja.starbux.dto.FinalOrderDto;
import com.nemanja.starbux.service.OrdersService;
import org.springframework.web.bind.annotation.*;


/**
 * Class that represent controller used for manipulating orders data.
 * Comes with one method that is used for finalizing the order
 *
 * @author Nemanja.Tomanovic
 */
@RestController
public class OrdersController {

    private OrdersService ordersService;

    public OrdersController(OrdersService ordersService) {
        this.ordersService = ordersService;
    }

    /**
     * Method used when we want to finalize order that we started
     * @param customer id of a customer that ordered drinks
     * @return response with original amount of order and discounted amount if customer was eligible for discount
     */
    @GetMapping("finish/orders/{customerId}")
    public FinalOrderDto finishOrder(@PathVariable("customerId") Long customer){
      return ordersService.createOrder(customer);
    }
}
