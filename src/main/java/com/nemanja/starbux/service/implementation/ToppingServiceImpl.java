package com.nemanja.starbux.service.implementation;

import com.nemanja.starbux.model.Toppings;
import com.nemanja.starbux.repositories.ToppingsRepository;
import com.nemanja.starbux.service.ToppingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Implementation of services defined in {@link com.nemanja.starbux.service.ToppingsService}
 *
 * Nemanja.Tomanovic created on 05-Mar-20
 **/
@Service
public class ToppingServiceImpl implements ToppingsService {

    private Logger logger = LoggerFactory.getLogger(ToppingServiceImpl.class);

    private ToppingsRepository toppingsRepository;

    public ToppingServiceImpl(ToppingsRepository toppingsRepository) {
        this.toppingsRepository = toppingsRepository;
    }

    @Override
    public Optional<Toppings> findTopping(Long id) {
        return toppingsRepository.findById(id);
    }

    @Override
    public Optional<Toppings> saveTopping(Toppings toppings) {
            logger.debug("Adding new topping : {}", toppings.getToppingName());
            toppingsRepository.save(toppings);
            return findTopping(toppings.getId());
    }

    @Override
    public boolean deleteToppingById(Long id) {
        try {
            logger.debug("Removing topping with id {}", id);
            toppingsRepository.deleteById(id);
            return true;
        }catch (Exception ex){
            logger.error("Exception during removal of topping");
            return false;
        }
    }

    @Override
    public boolean updateTopping(Toppings toppings) {
        if (checkUpdate(toppings)){
            try {
                logger.debug("Updating topping : {}", toppings.getToppingName());
                toppingsRepository.save(toppings);
                return true;
            }catch (Exception ex){
                logger.error("Exception during topping update");
                return false;
            }
        }else {
            logger.info("You need to pass existing topping for update");
            return false;
        }


    }

    /**
     * Method used for checking if topping is eligible for update.
     * Topping is eligible if already exists in database
     *
     * @param toppings toppings object with information regarding update
     *
     * @return true if topping is present in database and false if topping is not present in database
     */
    @Override
    public boolean checkUpdate(Toppings toppings){
        return findTopping(toppings.getId()).isPresent();
    }
}
